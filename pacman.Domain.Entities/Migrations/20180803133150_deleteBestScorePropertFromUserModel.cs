﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace pacman.Domain.Entities.Migrations
{
    public partial class deleteBestScorePropertFromUserModel : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "BestScore",
                table: "AspNetUsers");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "BestScore",
                table: "AspNetUsers",
                nullable: false,
                defaultValue: 0);
        }
    }
}
