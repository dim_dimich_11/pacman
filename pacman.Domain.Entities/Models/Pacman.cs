﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using pacman.Domain.Entities.Enums;
using pacman.Domain.Entities.Extensions;
using pacman.Domain.Entities.ViewModels;

namespace pacman.Domain.Entities.Models
{
    public class Pacman : Creature
    {
        public int Points { get; set; }

        public string UserId { get; set; }

        public PacmanColor Color { get; set; }

        public Pacman(int xCoordinate, int yCoordinate, string gameId, string userId, PacmanColor color, Direction direction) : base(xCoordinate,
            yCoordinate, gameId, CreatureType.Pacman, direction)
        {
            UserId = userId;
            Color = color;
        }

        public bool CanMove(Direction direction, Game game)
        {
            bool nextStepIsntWallOrPacman =
                !direction.NextFieldIsObstacle(XCoordinate, YCoordinate, FieldType.Wall, game) &&
                !direction.NextFieldIsObstacle(XCoordinate, YCoordinate, FieldType.Pacman, game);

            return nextStepIsntWallOrPacman;
        }

        public override void MoveByDirection(MoveResult moveResult, Game game)
        {
            if (Direction == Direction.None)
            {
                return;
            }

            bool nextStepGhost = Direction.NextFieldIsObstacle(XCoordinate, YCoordinate, FieldType.Ghost, game);
            bool nextStepWall = Direction.NextFieldIsObstacle(XCoordinate, YCoordinate, FieldType.Wall, game);
            bool nextStepPacman = Direction.NextFieldIsObstacle(XCoordinate, YCoordinate, FieldType.Pacman, game);

            if (nextStepGhost)
            {
                Direction = Direction.None;
                return;
            }

            if (nextStepWall || nextStepPacman)
            {
                Direction = Direction.None;
                return;
            }

            moveResult.NewX = Direction.GetNextXCoordinateValue(XCoordinate);
            moveResult.NewY = Direction.GetNextYCoordinateValue(YCoordinate);
            Points += game.Level[moveResult.NewX, moveResult.NewY] == FieldType.Point ? 2 : 0;
            game.GamePoints += Points;
            game.Level[XCoordinate, YCoordinate] = FieldType.Empty;
            game.Level[moveResult.NewX, moveResult.NewY] = FieldType.Pacman;
            XCoordinate = moveResult.NewX;
            YCoordinate = moveResult.NewY;
        }
    }
}
