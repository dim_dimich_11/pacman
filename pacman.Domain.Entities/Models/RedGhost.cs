﻿using System;
using System.Collections.Generic;
using System.Text;
using pacman.Domain.Entities.Enums;
using pacman.Domain.Entities.Extensions;
using pacman.Domain.Entities.Infrastructure;
using pacman.Domain.Entities.ViewModels;

namespace pacman.Domain.Entities.Models
{
    public class RedGhost : Creature, IGhost
    {
        public RedGhost(int xCoordinate, int yCoordinate, string gameId, CreatureType creatureType, Direction direction)
            : base(xCoordinate, yCoordinate, gameId, creatureType, direction)
        {

        }

        public override void MoveByDirection(MoveResult moveResult, Game game)
        {
            moveResult.GameOver = game.GameOver =
                Direction.NextFieldIsObstacle(XCoordinate, YCoordinate, FieldType.Pacman, game);
            
            moveResult.NewX = Direction.GetNextXCoordinateValue(XCoordinate);
            moveResult.NewY = Direction.GetNextYCoordinateValue(YCoordinate);
            game.Level[XCoordinate, YCoordinate] = FieldType.Point;
            game.Level[moveResult.NewX, moveResult.NewY] = FieldType.Ghost;
            XCoordinate = moveResult.NewX;
            YCoordinate = moveResult.NewY;
        }

        public void CheckDirection(Game game)
        {
            var nextStepIsWall = Direction.NextFieldIsObstacle(XCoordinate, YCoordinate, FieldType.Wall, game);

            switch (Direction)
            {
                case Direction.Left:
                    ContinueMoveLeft(game, nextStepIsWall);
                    break;
                case Direction.Top:
                    ContinueMoveTop(game, nextStepIsWall);
                    break;
                case Direction.Right:
                    ContinueMoveRight(game, nextStepIsWall);
                    break;
                case Direction.Bottom:
                    ContinueMoveBottom(game, nextStepIsWall);
                    break;
            }
        }

        private void ContinueMoveLeft(Game game, bool nextStepIsWall)
        {
            if (!nextStepIsWall)
            {
                if (!Direction.Top.NextFieldIsObstacle(XCoordinate, YCoordinate, FieldType.Wall, game) && DateTime.UtcNow.Ticks % 2 == 0)
                {
                    Direction = Direction.Top;
                    return;
                }

                Direction = Direction.Left;
            }
            else
            {

                if (!Direction.Bottom.NextFieldIsObstacle(XCoordinate, YCoordinate, FieldType.Wall, game) &&
                    Direction.Top.NextFieldIsObstacle(XCoordinate, YCoordinate, FieldType.Wall, game))
                {
                    Direction = Direction.Bottom;
                    return;
                }

                if (!Direction.Top.NextFieldIsObstacle(XCoordinate, YCoordinate, FieldType.Wall, game))
                {
                    Direction = Direction.Top;
                    return;
                }

                Direction = Direction.Right;
            }
        }

        private void ContinueMoveRight(Game game, bool nextStepIsWall)
        {
            if (!nextStepIsWall)
            {
                var nextStepNotWallAndXCoordinateIsEvenNumber =
                    !Direction.Bottom.NextFieldIsObstacle(XCoordinate, YCoordinate, FieldType.Wall, game) &&
                    YCoordinate % 2 != 0 && DateTime.UtcNow.Ticks % 2 != 0;

                Direction = nextStepNotWallAndXCoordinateIsEvenNumber
                    ? Direction.Bottom
                    : Direction.Right;
            }
            else
            {
                Direction = Direction.Bottom;
            }
        }

        private void ContinueMoveTop(Game game, bool nextStepIsWall)
        {
            if (!nextStepIsWall)
            {
                var stepLeftWallStepRightEmptyAndTickEvalNumber =
                    !Direction.Left.NextFieldIsObstacle(XCoordinate, YCoordinate, FieldType.Wall, game) &&
                    !Direction.Right.NextFieldIsObstacle(XCoordinate, YCoordinate, FieldType.Wall, game) &&
                    DateTime.UtcNow.Ticks % 2 != 0;

                if (stepLeftWallStepRightEmptyAndTickEvalNumber)
                {
                    Direction = Direction.Right;
                    return;
                }

                Direction = Direction.Top;
            }
            else
            {
                Direction = Direction.Left.NextFieldIsObstacle(XCoordinate, YCoordinate, FieldType.Wall, game)
                    ? Direction.Right
                    : Direction.Left;
            }
        }

        private void ContinueMoveBottom(Game game, bool nextStepIsWall)
        {
            if (!nextStepIsWall)
            {
                Direction = Direction.Bottom;
            }
            else
            {
                Direction = Direction.Left.NextFieldIsObstacle(XCoordinate, YCoordinate, FieldType.Wall, game)
                    ? Direction.Right
                    : Direction.Left;
            }
        }
    }
}
