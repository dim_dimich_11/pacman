﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using pacman.Domain.Entities.Enums;
using pacman.Domain.Entities.Extensions;
using pacman.Domain.Entities.Infrastructure;
using pacman.Domain.Entities.ViewModels;

namespace pacman.Domain.Entities.Models
{
    public class GreenGhost : Creature, IGhost
    {
        public GreenGhost(int xCoordinate, int yCoordinate, string gameId, CreatureType creatureType, Direction direction)
            : base(xCoordinate, yCoordinate, gameId, creatureType, direction)
        {
        }

        public override void MoveByDirection(MoveResult moveResult, Game game)
        {
            moveResult.GameOver = game.GameOver =
                Direction.NextFieldIsObstacle(XCoordinate, YCoordinate, FieldType.Pacman, game);

            moveResult.NewX = Direction.GetNextXCoordinateValue(XCoordinate);
            moveResult.NewY = Direction.GetNextYCoordinateValue(YCoordinate);
            game.Level[XCoordinate, YCoordinate] = FieldType.Point;
            game.Level[moveResult.NewX, moveResult.NewY] = FieldType.Ghost;
            XCoordinate = moveResult.NewX;
            YCoordinate = moveResult.NewY;
        }

        public void CheckDirection(Game game)
        {
            var pacmans = game.GameParticipants.Where(x =>
                x.CreatureType == CreatureType.Pacman).Select(x => (Pacman)x).ToList();
            var pathsList = new List<List<string>>();

            foreach (var pacman in pacmans)
            {
                pathsList.Add(FindShortWay(XCoordinate, YCoordinate, pacman.XCoordinate, pacman.YCoordinate, game));
            }

            int pathIndex = pathsList.Select((value, index) => new { Value = value, Index = index })
                .Aggregate((a, b) =>
                {
                    if (a.Value.Count == b.Value.Count)
                    {
                        return a;
                    }

                    return a.Value.Count > b.Value.Count ? b : a;
                })
                .Index;

            Direction = pathsList[pathIndex][0]
                .GetDirection(XCoordinate, YCoordinate);
        }

        private List<string> FindShortWay(int startXCoordinate, int startYCoordinate, int targetXCoordinate, int targetYCoordinate, Game game)
        {
            var path = new List<string>();
            var passedGraphTops = new Dictionary<string, string>();
            var distances = new Dictionary<string, int>();//all graph tops(key - top name, value - distance)
            var nodes = new List<string>();

            foreach (var vertex in game.GraphVertex)
            {
                if (vertex.Key == $"{startXCoordinate}_{startYCoordinate}")
                {
                    distances[vertex.Key] = 0;
                }
                else
                {
                    distances[vertex.Key] = int.MaxValue;
                }

                nodes.Add(vertex.Key);
            }

            while (nodes.Count != 0)
            {
                nodes.Sort((x, y) => distances[x] - distances[y]);

                var smallest = nodes[0];
                nodes.Remove(smallest);

                if (smallest == $"{targetXCoordinate}_{targetYCoordinate}")
                {
                    while (passedGraphTops.ContainsKey(smallest))
                    {
                        path.Insert(0, smallest);
                        smallest = passedGraphTops[smallest];
                    }

                    break;
                }

                if (distances[smallest] == int.MaxValue) //path not found
                {
                    break;
                }

                foreach (var neighbor in game.GraphVertex[smallest])
                {
                    var alt = distances[smallest] + neighbor.Value;//calculate count of distance from smallest top to neighbor

                    if (alt < distances[neighbor.Key])
                    {
                        distances[neighbor.Key] = alt;
                        passedGraphTops[neighbor.Key] = smallest;//add top to passed tops array
                    }
                }
            }

            return path;
        }
    }
}
