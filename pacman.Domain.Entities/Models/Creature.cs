﻿using System;
using System.Collections.Generic;
using System.Text;
using pacman.Domain.Entities.Enums;
using pacman.Domain.Entities.ViewModels;

namespace pacman.Domain.Entities.Models
{
    public abstract class Creature
    {
        protected Creature(int xCoordinate, int yCoordinate, string gameId, CreatureType creatureType,
            Direction direction)
        {
            XCoordinate = xCoordinate;
            YCoordinate = yCoordinate;
            GameId = gameId;
            CreatureType = creatureType;
            Direction = direction;
        }

        public int XCoordinate { get; set; }

        public int YCoordinate { get; set; }

        public CreatureType CreatureType { get; set; }

        public string GameId { get; set; }

        public Direction Direction { get; set; }

        public abstract void MoveByDirection(MoveResult moveResult, Game game);
    }
}
