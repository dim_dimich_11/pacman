﻿using System;
using System.Collections.Generic;
using System.Text;
using pacman.Domain.Entities.Enums;

namespace pacman.Domain.Entities.Models
{
    public class Game
    {
        public Game()
        {
            Rows = Level.GetUpperBound(0) + 1;
            Columns = Level.Length / Rows;
            GameSpeed = 300;
            BuildingGraph();
        }

        public string Id { get; set; } = Guid.NewGuid().ToString();

        public string OwnerId { get; set; }

        public List<Creature> GameParticipants { get; set; } = new List<Creature>();

        public Dictionary<string, Dictionary<string, int>> GraphVertex { get; set; } =
            new Dictionary<string, Dictionary<string, int>>();

        public GameType GameType { get; set; }

        public bool GameOver { get; set; }

        public bool PauseMode { get; set; }

        public int Rows { get; set; }

        public int Columns { get; set; }

        public int GameSpeed { get; set; }

        public int GamePoints
        {
            get => _gameSpeed;
            set
            {
                _gameSpeed = value;
                ChangeGameSpeed();
            }
        }

        private int _gameSpeed;

        public FieldType[,] Level { get; set; } =
        {
            {FieldType.Wall,FieldType.Wall,FieldType.Wall,FieldType.Wall,FieldType.Wall,FieldType.Wall,FieldType.Wall,FieldType.Wall,FieldType.Wall,FieldType.Wall,FieldType.Wall,FieldType.Wall,FieldType.Wall,FieldType.Wall,FieldType.Wall,FieldType.Wall,FieldType.Wall,FieldType.Wall,FieldType.Wall,FieldType.Wall},
            {FieldType.Wall,FieldType.Point,FieldType.Point,FieldType.Point,FieldType.Point,FieldType.Point,FieldType.Point,FieldType.Point,FieldType.Point,FieldType.Point,FieldType.Point,FieldType.Point,FieldType.Point,FieldType.Point,FieldType.Point,FieldType.Point,FieldType.Point,FieldType.Point,FieldType.Point,FieldType.Wall},
            {FieldType.Wall,FieldType.Point,FieldType.Wall,FieldType.Wall,FieldType.Wall,FieldType.Wall,FieldType.Wall,FieldType.Wall,FieldType.Wall,FieldType.Wall,FieldType.Wall,FieldType.Wall,FieldType.Wall,FieldType.Wall,FieldType.Wall,FieldType.Wall,FieldType.Wall,FieldType.Wall,FieldType.Point,FieldType.Wall},
            {FieldType.Wall,FieldType.Point,FieldType.Wall,FieldType.Point,FieldType.Point,FieldType.Point,FieldType.Point,FieldType.Point,FieldType.Point,FieldType.Point,FieldType.Point,FieldType.Point,FieldType.Point,FieldType.Point,FieldType.Point,FieldType.Point,FieldType.Point,FieldType.Point,FieldType.Point,FieldType.Wall},
            {FieldType.Wall,FieldType.Point,FieldType.Wall,FieldType.Point,FieldType.Wall,FieldType.Wall,FieldType.Wall,FieldType.Wall,FieldType.Point,FieldType.Wall,FieldType.Wall,FieldType.Wall,FieldType.Wall,FieldType.Wall,FieldType.Wall,FieldType.Wall,FieldType.Wall,FieldType.Wall,FieldType.Point,FieldType.Wall},
            {FieldType.Wall,FieldType.Point,FieldType.Wall,FieldType.Point,FieldType.Wall,FieldType.Point,FieldType.Point,FieldType.Point,FieldType.Point,FieldType.Point,FieldType.Point,FieldType.Point,FieldType.Point,FieldType.Point,FieldType.Point,FieldType.Point,FieldType.Point,FieldType.Wall,FieldType.Point,FieldType.Wall},
            {FieldType.Wall,FieldType.Point,FieldType.Wall,FieldType.Point,FieldType.Wall,FieldType.Point,FieldType.Wall,FieldType.Point,FieldType.Wall,FieldType.Wall,FieldType.Empty,FieldType.Wall,FieldType.Wall,FieldType.Point,FieldType.Wall,FieldType.Wall,FieldType.Point,FieldType.Wall,FieldType.Point,FieldType.Wall},
            {FieldType.Wall,FieldType.Point,FieldType.Wall,FieldType.Point,FieldType.Wall,FieldType.Point,FieldType.Wall,FieldType.Point,FieldType.Wall,FieldType.Empty,FieldType.Empty,FieldType.Empty,FieldType.Wall,FieldType.Point,FieldType.Wall,FieldType.Wall,FieldType.Point,FieldType.Wall,FieldType.Point,FieldType.Wall},
            {FieldType.Wall,FieldType.Point,FieldType.Point,FieldType.Point,FieldType.Wall,FieldType.Point,FieldType.Wall,FieldType.Point,FieldType.Wall,FieldType.Wall,FieldType.Wall,FieldType.Wall,FieldType.Wall,FieldType.Point,FieldType.Wall,FieldType.Wall,FieldType.Point,FieldType.Point,FieldType.Point,FieldType.Wall},
            {FieldType.Wall,FieldType.Point,FieldType.Wall,FieldType.Point,FieldType.Wall,FieldType.Point,FieldType.Wall,FieldType.Point,FieldType.Point,FieldType.Point,FieldType.Point,FieldType.Point,FieldType.Point,FieldType.Point,FieldType.Point,FieldType.Wall,FieldType.Point,FieldType.Wall,FieldType.Point,FieldType.Wall},
            {FieldType.Wall,FieldType.Point,FieldType.Wall,FieldType.Point,FieldType.Wall,FieldType.Point,FieldType.Point,FieldType.Point,FieldType.Wall,FieldType.Wall,FieldType.Wall,FieldType.Wall,FieldType.Wall,FieldType.Wall,FieldType.Point,FieldType.Point,FieldType.Point,FieldType.Wall,FieldType.Point,FieldType.Wall},
            {FieldType.Wall,FieldType.Point,FieldType.Wall,FieldType.Point,FieldType.Point,FieldType.Point,FieldType.Point,FieldType.Wall,FieldType.Wall,FieldType.Wall,FieldType.Wall,FieldType.Wall,FieldType.Wall,FieldType.Wall,FieldType.Wall,FieldType.Wall,FieldType.Point,FieldType.Wall,FieldType.Point,FieldType.Wall},
            {FieldType.Wall,FieldType.Point,FieldType.Wall,FieldType.Point,FieldType.Wall,FieldType.Wall,FieldType.Point,FieldType.Point,FieldType.Point,FieldType.Point,FieldType.Point,FieldType.Point,FieldType.Point,FieldType.Point,FieldType.Point,FieldType.Point,FieldType.Point,FieldType.Wall,FieldType.Point,FieldType.Wall},
            {FieldType.Wall,FieldType.Point,FieldType.Point,FieldType.Point,FieldType.Wall,FieldType.Wall,FieldType.Point,FieldType.Wall,FieldType.Point,FieldType.Wall,FieldType.Wall,FieldType.Wall,FieldType.Wall,FieldType.Wall,FieldType.Wall,FieldType.Wall,FieldType.Point,FieldType.Wall,FieldType.Point,FieldType.Wall},
            {FieldType.Wall,FieldType.Point,FieldType.Wall,FieldType.Point,FieldType.Point,FieldType.Point,FieldType.Point,FieldType.Wall,FieldType.Point,FieldType.Point,FieldType.Point,FieldType.Point,FieldType.Point,FieldType.Point,FieldType.Point,FieldType.Point,FieldType.Point,FieldType.Point,FieldType.Point,FieldType.Wall},
            {FieldType.Wall,FieldType.Point,FieldType.Wall,FieldType.Point,FieldType.Wall,FieldType.Wall,FieldType.Wall,FieldType.Wall,FieldType.Wall,FieldType.Wall,FieldType.Wall,FieldType.Point,FieldType.Wall,FieldType.Wall,FieldType.Wall,FieldType.Wall,FieldType.Wall,FieldType.Wall,FieldType.Point,FieldType.Wall},
            {FieldType.Wall,FieldType.Point,FieldType.Wall,FieldType.Point,FieldType.Point,FieldType.Point,FieldType.Point,FieldType.Wall,FieldType.Point,FieldType.Point,FieldType.Point,FieldType.Point,FieldType.Point,FieldType.Point,FieldType.Point,FieldType.Point,FieldType.Point,FieldType.Wall,FieldType.Point,FieldType.Wall},
            {FieldType.Wall,FieldType.Point,FieldType.Wall,FieldType.Wall,FieldType.Wall,FieldType.Wall,FieldType.Point,FieldType.Wall,FieldType.Point,FieldType.Wall,FieldType.Wall,FieldType.Wall,FieldType.Wall,FieldType.Point,FieldType.Wall,FieldType.Wall,FieldType.Point,FieldType.Wall,FieldType.Point,FieldType.Wall},
            {FieldType.Wall,FieldType.Point,FieldType.Point,FieldType.Point,FieldType.Point,FieldType.Point,FieldType.Point,FieldType.Point,FieldType.Point,FieldType.Point,FieldType.Point,FieldType.Point,FieldType.Point,FieldType.Point,FieldType.Wall,FieldType.Wall,FieldType.Point,FieldType.Point,FieldType.Point,FieldType.Wall},
            {FieldType.Wall,FieldType.Wall,FieldType.Wall,FieldType.Wall,FieldType.Wall,FieldType.Wall,FieldType.Wall,FieldType.Wall,FieldType.Wall,FieldType.Wall,FieldType.Wall,FieldType.Wall,FieldType.Wall,FieldType.Wall,FieldType.Wall,FieldType.Wall,FieldType.Wall,FieldType.Wall,FieldType.Wall,FieldType.Wall}
        };

        private void ChangeGameSpeed()
        {
            if (GamePoints > 0 && GamePoints % 250 == 0 && GameSpeed > 80)
            {
                GameSpeed -= 20;
            }
        }

        private void BuildingGraph()
        {
            for (var i = 0; i < Rows; i++)
            {
                for (var j = 0; j < Columns; j++)
                {
                    var field = Level[i, j];

                    if (field == FieldType.Wall)
                    {
                        continue;
                    }

                    var neighbors = new Dictionary<string, int>();

                    //check top direction
                    if (Level[i - 1, j] != FieldType.Wall)
                    {
                        neighbors.Add($"{i - 1}_{j}", 1);
                    }

                    //check right direction
                    if (Level[i, j + 1] != FieldType.Wall)
                    {
                        neighbors.Add($"{i}_{j + 1}", 1);
                    }

                    //check bottom direction
                    if (Level[i + 1, j] != FieldType.Wall)
                    {
                        neighbors.Add($"{i + 1}_{j}", 1);
                    }

                    //check left direction
                    if (Level[i, j - 1] != FieldType.Wall)
                    {
                        neighbors.Add($"{i}_{j - 1}", 1);
                    }
                    GraphVertex.Add($"{i}_{j}", neighbors);
                }
            }
        }
    }
}
