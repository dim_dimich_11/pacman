﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using pacman.Domain.Entities.Infrastructure;

namespace pacman.Domain.Entities.Models
{
    public class Score : BaseEntity
    {
        public string UserNickName { get; set; }

        public long Points { get; set; }
    }
}
