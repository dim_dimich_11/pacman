﻿using System;
using System.Collections.Generic;
using System.Text;

namespace pacman.Domain.Entities.ViewModels
{
    public class MoveResult
    {
        public int OldX { get; set; }

        public int OldY { get; set; }

        public int NewX { get; set; }

        public int NewY { get; set; }

        public bool GameOver { get; set; }
    }
}
