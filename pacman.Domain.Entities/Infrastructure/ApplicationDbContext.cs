﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using pacman.Domain.Entities.Models;
using Microsoft.EntityFrameworkCore;

namespace pacman.Domain.Entities.Infrastructure
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {
        }

        public DbSet<Score> Scores { get; set; }
    }
}
