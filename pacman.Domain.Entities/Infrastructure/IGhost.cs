﻿using System;
using System.Collections.Generic;
using System.Text;
using pacman.Domain.Entities.Models;

namespace pacman.Domain.Entities.Infrastructure
{
    public interface IGhost
    {
        void CheckDirection(Game game);
    }
}
