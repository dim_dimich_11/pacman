﻿using System;
using System.Collections.Generic;
using System.Text;

namespace pacman.Domain.Entities.Infrastructure
{
    public class DatabaseResult
    {
        public bool Success { get; set; } = false;
        public string Message { get; set; } = string.Empty;
        public Exception Exception { get; set; } = null;
    }
}
