﻿using System;
using System.Collections.Generic;
using System.Text;
using pacman.Domain.Entities.Enums;
using pacman.Domain.Entities.Models;

namespace pacman.Domain.Entities.Extensions
{
    public static class DirectionEnumExtension
    {
        public static bool NextFieldIsObstacle(this Direction direction, int xCoordinate, int yCoordinate,
            FieldType fieldType, Game game)
        {
            var nextStepIsObstacle = false;

            switch (direction)
            {
                case Direction.Left:
                    nextStepIsObstacle = game.Level[xCoordinate, yCoordinate - 1] == fieldType;
                    break;
                case Direction.Top:
                    nextStepIsObstacle = game.Level[xCoordinate - 1, yCoordinate] == fieldType;
                    break;
                case Direction.Right:
                    nextStepIsObstacle = game.Level[xCoordinate, yCoordinate + 1] == fieldType;
                    break;
                case Direction.Bottom:
                    nextStepIsObstacle = game.Level[xCoordinate + 1, yCoordinate] == fieldType;
                    break;
            }

            return nextStepIsObstacle;
        }

        public static int GetNextXCoordinateValue(this Direction direction, int xCoordinate)
        {
            switch (direction)
            {
                case Direction.Top:
                    xCoordinate -= 1;
                    break;
                case Direction.Bottom:
                    xCoordinate += 1;
                    break;
            }

            return xCoordinate;
        }

        public static int GetNextYCoordinateValue(this Direction direction, int yCoordinate)
        {
            switch (direction)
            {
                case Direction.Left:
                    yCoordinate -= 1;
                    break;
                case Direction.Right:
                    yCoordinate += 1;
                    break;
            }

            return yCoordinate;
        }
    }
}
