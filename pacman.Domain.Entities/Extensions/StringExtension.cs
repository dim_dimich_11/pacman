﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using pacman.Domain.Entities.Enums;

namespace pacman.Domain.Entities.Extensions
{
    public static class StringExtension
    {
        public static Direction GetDirection(this string vertexName, int currentXCoordinate, int currentYCoordinate)
        {
            var resultDirection = Direction.None;
            var coordinates = vertexName.Split('_').Select(int.Parse).ToList();
            var nextXCoordinate = coordinates[0];
            var nextYCoordinate = coordinates[1];

            if (nextXCoordinate > currentXCoordinate || nextXCoordinate < currentXCoordinate)
            {
                resultDirection = nextXCoordinate > currentXCoordinate ? Direction.Bottom : Direction.Top;
            }
            else
            {
                resultDirection = nextYCoordinate > currentYCoordinate ? Direction.Right : Direction.Left;
            }

            return resultDirection;
        }
    }
}
