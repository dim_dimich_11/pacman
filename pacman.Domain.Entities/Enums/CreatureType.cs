﻿using System;
using System.Collections.Generic;
using System.Text;

namespace pacman.Domain.Entities.Enums
{
    public enum CreatureType
    {
        None,
        Pacman,
        RedGhost,
        GreenGhost
    }
}
