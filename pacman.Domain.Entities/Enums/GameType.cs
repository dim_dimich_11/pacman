﻿using System;
using System.Collections.Generic;
using System.Text;

namespace pacman.Domain.Entities.Enums
{
    public enum GameType
    {
        Single,
        TwoPlayers
    }
}
