﻿using System;
using System.Collections.Generic;
using System.Text;

namespace pacman.Domain.Entities.Enums
{
    public enum FieldType
    {
        Empty = 1,
        Point,
        Wall,
        Pacman,
        Ghost,
        Cherry
    }
}
