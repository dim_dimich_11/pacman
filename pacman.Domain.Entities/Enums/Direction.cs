﻿using System;
using System.Collections.Generic;
using System.Text;

namespace pacman.Domain.Entities.Enums
{
    public enum Direction
    {
        Left = 37,
        Top,
        Right,
        Bottom,
        None
    }
}
