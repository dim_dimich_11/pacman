﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using pacman.Domain.Entities.Infrastructure;
using pacman.Domain.Entities.Models;

namespace pacman.Services.Interfaces
{
    public interface IScoreService
    {
        Task<DatabaseResult> AddScoreAsync(string userId, Score score);
        Task<List<Score>> GetScoresAsync(Expression<Func<Score, object>> sortedExpression, int limit, int skip);
    }
}
