﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using pacman.Domain.Entities.Enums;
using pacman.Domain.Entities.Models;

namespace pacman.Services.Interfaces
{
    public interface IGameService
    {
        Task MoveGhostAsync(string userId, CreatureType creatureType);
        Task ChangeGameModeAsync(string gameId, string userId);
        Task MovePacmanAsync(string userId);
        Task ChangeDirection(int directionCode, string userId);
        Task JoinToGameAsync(string userId, string userConnectionId, string gameId);
        Task<string> CreateGameAsync(string userId, GameType gameType, PacmanColor color);
        Task StartGameAsync(string userId, string gameId, bool runGhosts = false);
        Task<Game> GetGameByIdAsync(string gameId);
        Task DeleteGameAsync(string userConnectionId, string userId);
    }
}
