﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using pacman.Domain.Entities.Models;

namespace pacman.Services.Interfaces
{
    public interface IUserService
    {
        Task<ApplicationUser> GetUserByIdAsync(string userId);
        Task<ApplicationUser> GetUserByNickNameAsymc(string userNickName);
        Task<string> GetUserNickNameAsync(string userId);
        Task AddUserToCahcheAsync(string userEmail);
        Task<bool> ChekUserFromMultiplyLoginAsync(string userEmail);
        Task RemoveLoginDataAsync(string userEmail);
    }
}
