﻿using pacman.Domain.Entities.Enums;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using pacman.Domain.Entities.Models;
using pacman.Domain.Entities.ViewModels;

namespace pacman.Services.Interfaces
{
    public interface IRealtimeService
    {
        Task MovePacmanAsync(MoveResult moveResult, string userId, string gameId);
        Task GameOverAsync(string userId, bool winGame);
        Task GameStartedAsync(string gameId);
        Task DeleteConnectionIdFromGroupAsync(string connectionId, string groupName);
        Task CantMovePacmanAsync(int direction, string userId);
        Task GhostMovedAsync(string gameId, MoveResult moveResult, CreatureType creatureType);
        Task JoinedToGameAsync(string connectionId, Pacman pacman, Game game);
        Task SecondUserJoinedAsync(string userId, int xPosition, int yPosition);
    }
}
