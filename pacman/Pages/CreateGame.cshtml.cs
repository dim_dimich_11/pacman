﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting.Internal;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using pacman.Domain.Entities.Enums;
using pacman.Domain.Entities.Models;
using pacman.Services.Interfaces;

namespace pacman.Pages
{
    [Authorize]
    public class CreateGameModel : PageModel
    {
        public CreateGameModel(IGameService gameService, UserManager<ApplicationUser> userManager)
        {
            _gameService = gameService;
            _userManager = userManager;
        }

        [BindProperty]
        public GameType GameType { get; set; } = GameType.Single;

        [BindProperty]
        public PacmanColor Color { get; set; } = PacmanColor.Yellow;

        private readonly IGameService _gameService;

        private readonly UserManager<ApplicationUser> _userManager;

        public async Task<IActionResult> OnPostAsync()
        {
            var currentUserId = _userManager.GetUserId(User);
            var gameId = await _gameService.CreateGameAsync(currentUserId, GameType, Color);
            var url = Url.Page("Game", new { gameId });

            return Redirect(url);
        }
    }
}