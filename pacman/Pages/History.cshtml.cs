﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using pacman.Domain.Entities.Models;
using pacman.Services.Interfaces;

namespace pacman.Pages
{
    [Authorize]
    public class HistoryModel : PageModel
    {
        private readonly IScoreService _scoreService;

        public HistoryModel(IScoreService scoreService)
        {
            _scoreService = scoreService;
        }

        public List<Score> Scores { get; set; }

        public async Task OnGet()
        {
            Scores = await _scoreService.GetScoresAsync(x => x.Points, int.MaxValue, 0);
        }
    }
}