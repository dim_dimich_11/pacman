﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using pacman.Domain.Entities.Enums;
using pacman.Domain.Entities.Models;
using pacman.Services.Interfaces;

namespace pacman.Pages
{
    [Authorize]
    public class GameModel : PageModel
    {
        private readonly IGameService _gameService;
        private readonly IMapper _mapper;

        public GameModel(IGameService gameService, IMapper mapper)
        {
            _gameService = gameService;
            _mapper = mapper;
        }

        public string Id { get; set; }

        public int Rows { get; set; }

        public int Columns { get; set; }

        public string OwnerId { get; set; }

        public GameType GameType { get; set; }

        public bool GameOver { get; set; }

        public List<Creature> GameParticipants { get; set; }

        public Dictionary<string, Dictionary<string, int>> GraphVertex { get; set; } =
            new Dictionary<string, Dictionary<string, int>>();

        public FieldType[,] Level { get; set; }

        public async Task<IActionResult> OnGet(string gameId)
        {
            var game = await _gameService.GetGameByIdAsync(gameId);

            if (game == null)
            {
                return RedirectToPage("/CreateGame");
            }

            var userIds = game.GameParticipants.Select(x =>
            {
                if (x.GetType() == typeof(Pacman))
                {
                    return (Pacman)x;
                }

                return null;
            }).Distinct().Select(x => x?.UserId).ToList();

            if (game.GamePoints > 0 && !userIds.Contains(User.FindFirst(ClaimTypes.NameIdentifier).Value))
            {
                return RedirectToPage("/CreateGame");
            }

            Rows = game.Rows;
            Columns = game.Columns;
            GameOver = game.GameOver;
            GameType = game.GameType;
            GameParticipants = game.GameParticipants;
            GraphVertex = game.GraphVertex;
            Level = game.Level;
            OwnerId = game.OwnerId;
            Id = game.Id;

            return Page();
        }
    }
}