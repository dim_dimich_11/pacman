﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using pacman.Data.Infrastructure;
using pacman.Data.Interfaces;
using pacman.Models;

namespace pacman.Repositories
{
    public class UserRepository : GenericRepository<ApplicationUser>, IUserRepository
    {
        public UserRepository(DbContext context) : base(context)
        {
        }

        public async Task<ApplicationUser> GetUserAsync(string userId)
        {
            var result = await FindOneAsync(x => x.Id == userId);

            return result;
        }

        public async Task<ApplicationUser> GetUserByNickNameAsync(string nickName)
        {
            var result = await FindOneAsync(x => x.NickName.ToLower().Contains(nickName));

            return result;
        }
    }
}
