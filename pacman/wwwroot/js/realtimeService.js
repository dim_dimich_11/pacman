﻿const connection = new signalR.HubConnectionBuilder()
    .withUrl("/notificationHub")
    .build();

connection.start().then(function () {
    var url = new URL(window.location.href);
    var gameId = url.searchParams.get("gameId");
    console.log("Connect to hub!!!!!");
    connection.invoke("JoinToGameAsync", gameId);
}, function onError(err) {
    console.error(err.toString());
});

var test = { a: "hhello" };