﻿var enteredKey = 0;
var game = {};
var pacman = {};
var gameMap = new Array();
var userScore = null;
var secondUserJoined = false;

$(document).ready(function () {
    $('#pausedModal').on('hidden.bs.modal', () => {
        if (!game.gameOver && game.pauseMode) {
            continuePlay();
        }
    });

    $('#gameOverModal').on('hidden.bs.modal', () => {
        connection.invoke("GameOverAsync", game.id, { points: pacman.points });
        game.gameOver = true;
    });

    $("#winGameModal").on('hidden.bs.modal', () => {
        connection.invoke("GameOverAsync", game.id, { points: pacman.points });
        game.gameOver = true;
    });
});

$(window).on("beforeunload",
    function () {
        if (!game.gameOver) {
            connection.invoke("GameOverAsync", game.id, { points: pacman.points });
            game.gameOver = true;
        }
    });

connection.on("JoinToGame",
    (gameModel, pacmanModel) => {
        game = gameModel;
        pacman = pacmanModel;
        getMap(game.rows, game.columns);

        if (!game.started) {
            for (let i = 0; i < game.gameParticipants.length; i++) {
                let participant = game.gameParticipants[i];
                let participantPosition = game.level[participant.xCoordinate][participant.yCoordinate];

                let participanElement = generateParticipantElement(participant);
                participantPosition.classList = participanElement.wrapperClassList;
                participantPosition.childNodes[0].classList = participanElement.childClassList;
            }

            document.getElementById("points").innerHTML = "Points: " + pacman.points;
            document.getElementById("modal-score").innerHTML = "You score is: " + pacman.points;
            secondUserJoined = game.gameType === 0 || game.ownerId !== pacman.userId;
        }
    });

connection.on("SecondUserJoined",
    (x, y) => {
        var secondPacman = game.level[x][y];;
        secondPacman.id = x + "-" + y;
        secondPacman.classList = ["pacman-wrapper"];
        secondPacman.childNodes[0].classList = pacman.color === 0 ? ["orange-pacman"] : ["pacman"];
        secondUserJoined = true;
    });

connection.on("PacmanMove", (position, userId) => {
    var oldPacman = game.level[position.oldX][position.oldY];

    oldPacman.classList = ["empty-field"];
    oldPacman.childNodes[0].classList = []; //remove all classes in child node
    oldPacman.childNodes[0].id = "";

    var newPacman = game.level[position.newX][position.newY];

    if (newPacman.childNodes[0].id.includes("point")) {
        pacman.points += pacman.userId === userId ? 2 : 0;
        document.getElementById("points").innerHTML = "Points: " + pacman.points;
        document.getElementById("modal-score").innerHTML = "You score is: " + pacman.points;
        document.getElementById("win-modal-score").innerHTML = "You score is: " + pacman.points;
    }

    newPacman.classList = ["pacman-wrapper"];

    if (pacman.userId === userId) {
        newPacman.childNodes[0].classList = getPacmanColorStyle(pacman.color);
    } else {
        newPacman.childNodes[0].classList = pacman.color === 0 ? ["orange-pacman"] : ["pacman"];
    }
});

connection.on("GhostMoved",
    (position, ghostType) => {
        var oldGhostPosition = game.level[position.oldX][position.oldY];

        oldGhostPosition.classList = ["point-wrapper"];
        oldGhostPosition.childNodes[0].classList = ["point"];
        oldGhostPosition.childNodes[0].id = "point";

        var newGhostPosition = game.level[position.newX][position.newY];
        var ghostElement = generateGhostElement(ghostType);
        newGhostPosition.classList = ghostElement.wrapperClass;
        newGhostPosition.childNodes[0].classList = ghostElement.childClass;
    });

connection.on("CantMove",
    (oldCode) => {
        enteredKey = oldCode;
    });

connection.on("GameOver",
    (winGame) => {
        game.gameOver = true;

        if (winGame) {
            $("#winGameModal").modal();
        } else {
            $("#gameOverModal").modal();
        }
    });

connection.on("GameStarted",
    () => {
        game.started = true;
    });

//37-left
//38-top
//39-right
//40-bottom
document.addEventListener("keydown", event => {

    if ((event.ctrlKey && event.keyCode === 116) || (event.ctrlKey && event.keyCode === 82) || event.keyCode === 116) {
        event.preventDefault();
    }

    if (event.keyCode === 27 && game.gameType === 0) {
        connection.invoke("ChangeGameModeAsync", game.id);
        game.pauseMode = true;
        $("#pausedModal").modal();

        return;
    }

    if ((event.keyCode === 40 || event.keyCode === 38 || event.keyCode === 37 || event.keyCode === 39) && !game.gameOver && !game.pauseMode) {
        if (enteredKey !== event.keyCode && (game.started || (secondUserJoined && game.ownerId === pacman.userId))) {
            connection.invoke("ChangeDirection", event.keyCode);

            if (enteredKey === 0) {
                connection.invoke("StartGame", game.id, game.ownerId === pacman.userId && !game.started);
            }

            enteredKey = event.keyCode;
            event.preventDefault();
        }
    }
});

document.getElementById("modalContinueGameButton").addEventListener("click", continuePlay);

document.getElementById("modalGameOverButton").addEventListener("click", function () {
    connection.invoke("GameOverAsync", game.id, { points: pacman.points });
    game.gameOver = true;
});

function getPacmanColorStyle(color) {
    switch (color) {
        case 0:
            return ["pacman"];
        case 1:
            return ["orange-pacman"];
    }
}

function generateParticipantElement(participant) {
    var element = { id: participant.xCoordinate + "-" + participant.yCoordinate };

    switch (participant.creatureType) {
        case 1:
            element.wrapperClassList = ["pacman-wrapper"];
            element.childClassList = getPacmanColorStyle(participant.color);
            break;
        case 2:
            element.wrapperClassList = ["red-ghost-wrapper"];
            element.childClassList = ["red-ghost"];
            break;
        case 3:
            element.wrapperClassList = ["green-ghost-wrapper"];

            element.childClassList = ["green-ghost"];

            break;
    }

    return element;
}

function generateGhostElement(ghostType) {

    var element = {};

    switch (ghostType) {
        case 2:
            element.wrapperClass = ["red-ghost-wrapper"];
            element.childClass = ["red-ghost"];

            break;
        case 3:
            element.wrapperClass = ["green-ghost-wrapper"];
            element.childClass = ["green-ghost"];

            break;
    }

    return element;
}

function continuePlay() {
    connection.invoke("ChangeGameModeAsync", game.id);
    game.pauseMode = false;
}

function getMap(rows, columns) {
    for (let i = 0; i < rows; i++) {
        for (let j = 0; j < columns; j++) {
            game.level[i][j] = document.getElementById(i + '-' + j);
        }
    }
}