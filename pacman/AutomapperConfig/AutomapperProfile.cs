﻿using AutoMapper;
using pacman.Domain.Entities.Models;

namespace pacman.AutomapperConfig
{
    public class AutomapperProfile : Profile
    {
        public AutomapperProfile()
        {
            CreateMap<Game, Pacman>().ForMember(x => x.UserId, opt => opt.UseValue("done")).ReverseMap();
        }
    }
}
