﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;
using pacman.Enums;
using pacman.Hubs;
using pacman.Interfaces;
using pacman.Models;
using pacman.Pages;

namespace pacman.Services
{
    public class RealtimeService : IRealtimeService
    {
        private readonly IHubContext<NotificationHub> _hubContext;

        public RealtimeService(IHubContext<NotificationHub> hubContext)
        {
            _hubContext = hubContext;
        }

        public async Task MovePacmanAsync(MoveResult moveResult, string userId, string gameId)
        {
            await _hubContext.Clients.Group(gameId).SendAsync("PacmanMove", moveResult, userId);
        }

        public async Task GameOverAsync()
        {
            await _hubContext.Clients.All.SendAsync("GameOver");
        }

        public async Task CantMovePacmanAsync(Direction direction, string userId)
        {
            await _hubContext.Clients.User(userId).SendAsync("CantMove", direction);
        }

        public async Task GameCreatedAsync(GameModel game)
        {
            await _hubContext.Clients.Group(game.Id).SendAsync("GameCreated", game);
        }

        public async Task GameStartedAsync(GameModel game)
        {
            var pacmans = game.GameParticipants.Where(x => x.CreatureType == CreatureType.Pacman).ToList();

            foreach (Pacman pacman in pacmans)
            {
                await _hubContext.Clients.Client(pacman.UserId).SendAsync("GameCreated", game);
            }
        }

        public async Task ReadGhostMovedAsync(MoveResult moveResult, GhostType ghostType)
        {
            await _hubContext.Clients.All.SendAsync("RedGhostMoved", moveResult, ghostType);
        }

        public async Task JoinedToGameAsync(string connectionId, Pacman pacman, Game game)
        {
            await _hubContext.Groups.AddToGroupAsync(connectionId, game.Id);

            await _hubContext.Clients.User(pacman.UserId).SendAsync("JoinToGame", game, pacman);
        }

        public async Task SecondUserJoinedAsync(string userId, int xPosition, int yPosition)
        {
            await _hubContext.Clients.User(userId).SendAsync("SecondUserJoined", xPosition, yPosition);
        }
    }
}
