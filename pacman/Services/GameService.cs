﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Caching.Memory;
using pacman.Enums;
using pacman.Interfaces;
using pacman.Models;
using pacman.Pages;

namespace pacman.Services
{
    public class GameService : IGameService
    {
        private readonly IMemoryCache _memoryCache;
        private readonly IRealtimeService _realtimeService;

        public GameService(IMemoryCache memoryCache, IRealtimeService realtimeService)
        {
            _memoryCache = memoryCache;
            _realtimeService = realtimeService;
        }

        public async Task MoveGhostAsync(string gameId)
        {
            while (true)
            {
                var game = (Game)_memoryCache.Get(gameId);

                if (game.GameOver)
                {
                    return;
                }

                var redGhost = (RedGhost)game.GameParticipants.Find(x => x.CreatureType == CreatureType.RedGhost);

                var moveResult = new MoveResult
                {
                    OldX = redGhost.XCoordinate,
                    OldY = redGhost.YCoordinate,
                    NewY = redGhost.YCoordinate,
                    NewX = redGhost.XCoordinate
                };

                redGhost.CheckDirection(game);
                redGhost.Move(game, moveResult);
                _memoryCache.Set(game.Id, game);

                if (game.GameOver)
                {
                    await Task.Delay(200);
                    //_memoryCache.Remove(game.Id);
                    await _realtimeService.GameOverAsync();
                    return;
                }

                await Task.Delay(game.GameSpeed);
                await _realtimeService.ReadGhostMovedAsync(moveResult, GhostType.RedGhost);
            }
        }

        public async Task MovePacmanAsync(string userId)
        {
            while (true)
            {
                var pacman = (Pacman)_memoryCache.Get(userId);
                var game = (Game)_memoryCache.Get(pacman.GameId);

                if (game.GameOver)
                {
                    pacman.GameId = null;
                    _memoryCache.Set(userId, pacman);
                    return;
                }

                var moveResult = new MoveResult
                {
                    OldX = pacman.XCoordinate,
                    OldY = pacman.YCoordinate
                };

                pacman.Move(game, moveResult);
                _memoryCache.Set(game.Id, game);

                if (game.GameOver)
                {
                    //await Task.Delay(200);
                    //await _realtimeService.GameOverAsync();
                    pacman.GameId = null;
                    _memoryCache.Set(userId, pacman);
                    return;
                }

                if (moveResult.NewX != 0 && moveResult.NewY != 0)
                {
                    _memoryCache.Set(userId, pacman);
                    await Task.Delay(200);
                    await _realtimeService.MovePacmanAsync(moveResult, pacman.UserId, game.Id);
                }
            }
        }

        public async Task<string> CreateGameAsync(string userId, GameType gameType, PacmanColor color)
        {
            var existPacman = (Pacman)_memoryCache.Get(userId);

            if (existPacman?.GameId == null)
            {
                var game = new Game
                {
                    OwnerId = userId,
                    GameType = gameType
                };

                var pacman = AddPacmanToGame(1, 1, userId, game.Id, color, Direction.None, game);

                AddGhostsToGame(game);
                _memoryCache.Set(game.Id, game, DateTimeOffset.UtcNow.AddMinutes(20));
                _memoryCache.Set(userId, pacman, DateTimeOffset.UtcNow.AddMinutes(20));

                return game.Id;
            }

            return existPacman.GameId;
        }

        public Game GetGameByIdAsync(string gameId)
        {
            var game = (Game)_memoryCache.Get(gameId);

            return game;
        }

        public void DeleteGame(string gameId)
        {
            _memoryCache.Remove(gameId);
        }

        public async Task JoinToGameAsync(string userId, string userConnectionId, string gameId)
        {
            var game = (Game)_memoryCache.Get(gameId);

            if (!_memoryCache.TryGetValue(userId, out Pacman pacman))
            {
                var anotherPacmanColor = (Pacman)game.GameParticipants.First(x => x.GetType() == typeof(Pacman));
                var myColor = anotherPacmanColor.Color == PacmanColor.Orange ? PacmanColor.Yellow : PacmanColor.Orange;

                pacman = AddPacmanToGame(18, 18, userId, gameId, myColor, Direction.None, game);

                _memoryCache.Set(userId, pacman);
                _memoryCache.Set(game.Id, game);

                await _realtimeService.JoinedToGameAsync(userConnectionId, pacman, game);
                await _realtimeService.SecondUserJoinedAsync(game.OwnerId, pacman.XCoordinate, pacman.YCoordinate);

                return;
            }

            await _realtimeService.JoinedToGameAsync(userConnectionId, pacman, game);
        }

        private static void AddGhostsToGame(Game game)
        {
            var redGhost = new RedGhost(5, 11, game.Id, CreatureType.RedGhost, Direction.Left);
            var greenGhost = new GreenGhost(7, 10, game.Id, CreatureType.GreenGhost, Direction.Top);

            game.Level[redGhost.XCoordinate, redGhost.YCoordinate] = FieldType.Ghost;
            game.Level[greenGhost.XCoordinate, greenGhost.YCoordinate] = FieldType.Ghost;
            game.GameParticipants.Add(redGhost);
            game.GameParticipants.Add(greenGhost);
        }

        private static Pacman AddPacmanToGame(int xCoordinate, int yCoordinate, string userId, string gameId,
            PacmanColor color, Direction direction, Game game)
        {
            var pacman = new Pacman(xCoordinate, yCoordinate, gameId, userId, color, direction);
            game.Level[xCoordinate, yCoordinate] = FieldType.Pacman;
            game.GameParticipants.Add(pacman);

            return pacman;
        }

        public async Task MoveGreenGhostAsync(string gameId)
        {
            while (true)
            {
                var game = (Game)_memoryCache.Get(gameId);

                if (game.GameOver)
                {
                    return;
                }

                var greenGhost = (GreenGhost)game.GameParticipants.Find(x => x.CreatureType == CreatureType.GreenGhost);

                var moveResult = new MoveResult
                {
                    OldX = greenGhost.XCoordinate,
                    OldY = greenGhost.YCoordinate,
                    NewY = greenGhost.YCoordinate,
                    NewX = greenGhost.XCoordinate
                };

                greenGhost.CheckDirection(game);
                greenGhost.Move(game, moveResult);
                _memoryCache.Set(game.Id, game);

                if (game.GameOver)
                {
                    await Task.Delay(200);
                    //_memoryCache.Remove(game.Id);
                    await _realtimeService.GameOverAsync();
                    return;
                }

                await Task.Delay(game.GameSpeed);
                await _realtimeService.ReadGhostMovedAsync(moveResult, GhostType.GreenGhost);
            }
        }
    }
}
