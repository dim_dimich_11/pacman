﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using pacman.Data.Interfaces;
using pacman.Interfaces;
using pacman.Models;

namespace pacman.Services
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;

        public UserService(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public async Task<ApplicationUser> GetUserByIdAsync(string userId)
        {
            var result = await _userRepository.GetUserAsync(userId);

            return result;
        }

        public async Task<ApplicationUser> GetUserByNickNameAsymc(string userNickName)
        {
            var result = await _userRepository.GetUserByNickNameAsync(userNickName.ToLower());

            return result;
        }
    }
}
