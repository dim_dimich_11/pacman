﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using pacman.Domain.Entities.Infrastructure;
using pacman.Domain.Entities.Models;
using pacman.Domain.Interfaces;
using pacman.Domain.Interfaces.Infrastructure;

namespace pacman.Infrastructure.Data
{
    public class ScoreRepository : GenericRepository<Score>, IScoreRepository
    {
        public ScoreRepository(DbContext context) : base(context)
        {
        }

        public async Task<DatabaseResult> AddScoreAsync(Score score)
        {
            var result = await InsertOneAsync(score);

            return result;
        }

        public async Task<List<Score>> GetScoresAsync(Expression<Func<Score, object>> sortedExpression, int limit, int skip)
        {
            var result = await FindManyWithOrderByDescendingAsync(x => true, sortedExpression, limit, skip);

            return result;
        }
    }
}
