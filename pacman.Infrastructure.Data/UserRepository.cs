﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using pacman.Domain.Entities.Infrastructure;
using pacman.Domain.Entities.Models;
using pacman.Domain.Interfaces;
using pacman.Domain.Interfaces.Infrastructure;

namespace pacman.Infrastructure.Data
{
    public class UserRepository : GenericRepository<ApplicationUser>, IUserRepository
    {
        public UserRepository(DbContext context) : base(context)
        {
        }

        public async Task<ApplicationUser> GetUserAsync(string userId)
        {
            var result = await FindOneAsync(x => x.Id == userId);

            return result;
        }

        public async Task<ApplicationUser> GetUserByNickNameAsync(string nickName)
        {
            var result = await FindOneAsync(x => x.NickName.ToLower().Contains(nickName));

            return result;
        }

        public async Task<DatabaseResult> UpdateUserAsync(ApplicationUser user)
        {
            var result = await UpdateOneAsync(user);

            return result;
        }

        public async Task<string> GetUserNickNameAsync(string userId)
        {
            var result = await FindOneAsync(x => x.Id == userId, x => x.NickName);

            return result;
        }
    }
}
