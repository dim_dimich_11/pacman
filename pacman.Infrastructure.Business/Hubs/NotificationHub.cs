﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Caching.Memory;
using pacman.Domain.Entities.Enums;
using pacman.Domain.Entities.Models;
using pacman.Domain.Interfaces;
using pacman.Services.Interfaces;

namespace pacman.Infrastructure.Business.Hubs
{
    [Authorize]
    public class NotificationHub : Hub
    {
        private readonly IGameService _gameService;
        private readonly IScoreService _scoreService;

        public NotificationHub(IGameService gameService, IScoreService scoreService)
        {
            _gameService = gameService;
            _scoreService = scoreService;
        }

        public override async Task OnConnectedAsync()
        {
            await base.OnConnectedAsync();
        }

        public async Task JoinToGameAsync(string gameId)
        {
            var userId = Context.User.FindFirst(ClaimTypes.NameIdentifier).Value;
            var userConnectionId = Context.ConnectionId;

            await _gameService.JoinToGameAsync(userId, userConnectionId, gameId);
        }

        public async Task StartGame(string gameId, bool runGhosts = false)
        {
            var userId = Context.User.FindFirst(ClaimTypes.NameIdentifier).Value;

            await _gameService.StartGameAsync(userId, gameId, runGhosts);
        }

        public async Task GameOverAsync(string gameId, Score score)
        {
            var userId = Context.User.FindFirst(ClaimTypes.NameIdentifier).Value;
            var userConnectionId = Context.ConnectionId;

            Task.Run(async () => { await _gameService.DeleteGameAsync(userConnectionId, userId); });

            await _scoreService.AddScoreAsync(userId, score);
        }

        public async Task ChangeGameModeAsync(string gameId)
        {
            var userId = Context.User.FindFirst(ClaimTypes.NameIdentifier).Value;

            await _gameService.ChangeGameModeAsync(gameId, userId);
        }

        public async Task SaveUserScoreAsync(Score score)
        {
            var userId = Context.User.FindFirst(ClaimTypes.NameIdentifier).Value;

            await _scoreService.AddScoreAsync(userId, score);
        }

        public async Task ChangeDirection(int code)
        {
            var userId = Context.User.FindFirst(ClaimTypes.NameIdentifier)?.Value;
            await _gameService.ChangeDirection(code, userId);
        }
    }
}
