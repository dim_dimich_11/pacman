﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;
using pacman.Domain.Entities.Enums;
using pacman.Domain.Entities.Models;
using pacman.Domain.Entities.ViewModels;
using pacman.Infrastructure.Business.Hubs;
using pacman.Services.Interfaces;

namespace pacman.Infrastructure.Business
{
    public class RealtimeService : IRealtimeService
    {
        private readonly IHubContext<NotificationHub> _hubContext;

        public RealtimeService(IHubContext<NotificationHub> hubContext)
        {
            _hubContext = hubContext;
        }

        public async Task MovePacmanAsync(MoveResult moveResult, string userId, string gameId)
        {
            await _hubContext.Clients.Group(gameId).SendAsync("PacmanMove", moveResult, userId);
        }

        public async Task GameOverAsync(string userId, bool winGame)
        {
            await _hubContext.Clients.User(userId).SendAsync("GameOver", winGame);
        }

        public async Task CantMovePacmanAsync(int direction, string userId)
        {
            await _hubContext.Clients.User(userId).SendAsync("CantMove", direction);
        }

        public async Task GhostMovedAsync(string gameId, MoveResult moveResult, CreatureType creatureType)
        {
            await _hubContext.Clients.Group(gameId).SendAsync("GhostMoved", moveResult, creatureType);
        }

        public async Task JoinedToGameAsync(string connectionId, Pacman pacman, Game game)
        {
            await _hubContext.Groups.AddToGroupAsync(connectionId, game.Id);

            await _hubContext.Clients.User(pacman.UserId).SendAsync("JoinToGame", game, pacman);
        }

        public async Task DeleteConnectionIdFromGroupAsync(string connectionId, string groupName)
        {
            await _hubContext.Groups.RemoveFromGroupAsync(connectionId, groupName);
        }

        public async Task SecondUserJoinedAsync(string userId, int xPosition, int yPosition)
        {
            await _hubContext.Clients.User(userId).SendAsync("SecondUserJoined", xPosition, yPosition);
        }

        public async Task GameStartedAsync(string gameId)
        {
            await _hubContext.Clients.Group(gameId).SendAsync("GameStarted");
        }
    }
}
