﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Caching.Memory;
using pacman.Domain.Entities.Models;
using pacman.Domain.Interfaces;
using pacman.Services.Interfaces;

namespace pacman.Infrastructure.Business
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;
        private readonly IMemoryCache _memoryCache;

        public UserService(IUserRepository userRepository, IMemoryCache memoryCache)
        {
            _userRepository = userRepository;
            _memoryCache = memoryCache;
        }

        public async Task AddUserToCahcheAsync(string userEmail)
        {
            await Task.Run(() => _memoryCache.Set(userEmail, DateTimeOffset.UtcNow.AddMinutes(30)));
        }

        public async Task<bool> ChekUserFromMultiplyLoginAsync(string userEmail)
        {
            var lasLoginTime = await Task.Run(() =>
            {
                _memoryCache.TryGetValue(userEmail, out DateTimeOffset? dateTimeOffset);

                return dateTimeOffset;
            });

            var isMultiplyLogin = lasLoginTime != null && DateTimeOffset.UtcNow <= lasLoginTime;

            return isMultiplyLogin;
        }

        public async Task<ApplicationUser> GetUserByIdAsync(string userId)
        {
            var result = await _userRepository.GetUserAsync(userId);

            return result;
        }

        public async Task<ApplicationUser> GetUserByNickNameAsymc(string userNickName)
        {
            var result = await _userRepository.GetUserByNickNameAsync(userNickName.ToLower());

            return result;
        }

        public async Task<string> GetUserNickNameAsync(string userId)
        {
            var result = await _userRepository.GetUserNickNameAsync(userId);

            return result;
        }

        public async Task RemoveLoginDataAsync(string userEmail)
        {
            await Task.Run(() => _memoryCache.Remove(userEmail));
        }
    }
}
