﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Caching.Memory;
using pacman.Domain.Entities.Enums;
using pacman.Domain.Entities.Models;
using pacman.Domain.Entities.ViewModels;
using pacman.Domain.Interfaces;
using pacman.Services.Interfaces;

namespace pacman.Infrastructure.Business
{
    public class GameService : IGameService
    {
        private readonly IMemoryCache _memoryCache;
        private readonly IRealtimeService _realtimeService;
        private readonly IUserRepository _userRepository;

        public GameService(IMemoryCache memoryCache, IRealtimeService realtimeService, IUserRepository userRepository)
        {
            _memoryCache = memoryCache;
            _realtimeService = realtimeService;
            _userRepository = userRepository;
        }

        public async Task MovePacmanAsync(string userId)
        {
            while (true)
            {
                var pacman = (Pacman)_memoryCache.Get(userId);
                var game = (Game)_memoryCache.Get(pacman.GameId);

                if (game.GameOver || game.PauseMode)
                {
                    return;
                }

                if (pacman.Direction == Direction.None)
                {
                    await _realtimeService.CantMovePacmanAsync(0, pacman.UserId);
                    return;
                }

                var moveResult = new MoveResult
                {
                    OldX = pacman.XCoordinate,
                    OldY = pacman.YCoordinate
                };

                pacman.MoveByDirection(moveResult, game);
                _memoryCache.Set(game.Id, game);
                _memoryCache.Set(userId, pacman);

                if (moveResult.NewX == 0 || moveResult.NewY == 0)
                {
                    await Task.Delay(200);
                    continue;
                }

                if (moveResult.GameOver)
                {
                    await GameOverAsync(pacman.UserId, game);
                    return;
                }

                await Task.Delay(200);
                await _realtimeService.MovePacmanAsync(moveResult, pacman.UserId, game.Id);
            }
        }

        public async Task MoveGhostAsync(string gameId, CreatureType creatureType)
        {
            while (true)
            {
                dynamic ghost;

                var game = (Game)_memoryCache.Get(gameId);

                if (game.GameOver || game.PauseMode)
                {
                    return;
                }

                if (creatureType == CreatureType.GreenGhost)
                {
                    ghost = (GreenGhost)game.GameParticipants.Find(x => x.CreatureType == creatureType);
                }
                else
                {
                    ghost = (RedGhost)game.GameParticipants.Find(x => x.CreatureType == creatureType);
                }

                var moveResult = new MoveResult
                {
                    OldX = ghost.XCoordinate,
                    OldY = ghost.YCoordinate,
                    NewY = ghost.YCoordinate,
                    NewX = ghost.XCoordinate
                };

                ghost.CheckDirection(game);
                ghost.MoveByDirection(moveResult, game);

                _memoryCache.Set(game.Id, game);

                await _realtimeService.GhostMovedAsync(game.Id, moveResult, creatureType);
                await Task.Delay(game.GameSpeed);

                if (moveResult.GameOver)
                {
                    string userId = GetPacmanId(moveResult.NewX, moveResult.NewY, game);
                    await GameOverAsync(userId, game);
                    return;
                }
            }
        }

        public async Task ChangeDirection(int directionCode, string userId)
        {
            if (Enum.IsDefined(typeof(Direction), directionCode))
            {
                var direction = (Direction)Enum.ToObject(typeof(Direction), directionCode);
                var pacman = (Pacman)_memoryCache.Get(userId);
                var game = (Game)_memoryCache.Get(pacman.GameId);
                var canMove = pacman.CanMove(direction, game);

                if (canMove)
                {
                    pacman.Direction = direction;
                    _memoryCache.Set(userId, pacman);
                }
                else
                {
                    await _realtimeService.CantMovePacmanAsync((int)pacman.Direction, userId);
                }
            }
        }

        public async Task StartGameAsync(string userId, string gameId, bool runGhosts = false)
        {
            Task.Run(async () => { await MovePacmanAsync(userId); });

            if (runGhosts)
            {
                await RunGhostAsync(gameId);
                await _realtimeService.GameStartedAsync(gameId);
            }
        }

        public async Task<string> CreateGameAsync(string userId, GameType gameType, PacmanColor color)
        {
            var existPacman = (Pacman)_memoryCache.Get(userId);

            if (existPacman?.GameId == null)
            {
                var game = new Game
                {
                    OwnerId = userId,
                    GameType = gameType
                };

                var pacman = AddPacmanToGame(1, 1, userId, color, Direction.None, game);

                AddGhostsToGame(game, pacman.UserId);

                await Task.Run(() =>
                {
                    _memoryCache.Set(game.Id, game, DateTimeOffset.UtcNow.AddMinutes(20));
                    _memoryCache.Set(userId, pacman, DateTimeOffset.UtcNow.AddMinutes(20));
                });

                return game.Id;
            }

            return existPacman.GameId;
        }

        public async Task<Game> GetGameByIdAsync(string gameId)
        {
            var game = (Game)await Task.Run(() => _memoryCache.Get(gameId));

            return game;
        }

        public async Task DeleteGameAsync(string userConnectionId, string userId)
        {
            var pacman = (Pacman)_memoryCache.Get(userId);
            await _realtimeService.DeleteConnectionIdFromGroupAsync(userConnectionId, pacman.GameId);
            _memoryCache.Remove(pacman.GameId);
            _memoryCache.Remove(userId);
        }

        public async Task JoinToGameAsync(string userId, string userConnectionId, string gameId)
        {
            var game = (Game)_memoryCache.Get(gameId);

            if (!_memoryCache.TryGetValue(userId, out Pacman pacman))
            {
                var anotherPacmanColor = (Pacman)game.GameParticipants.First(x => x.GetType() == typeof(Pacman));
                var myColor = anotherPacmanColor.Color == PacmanColor.Orange ? PacmanColor.Yellow : PacmanColor.Orange;

                pacman = AddPacmanToGame(18, 18, userId, myColor, Direction.None, game);

                _memoryCache.Set(userId, pacman);
                _memoryCache.Set(game.Id, game);

                await _realtimeService.JoinedToGameAsync(userConnectionId, pacman, game);
                await _realtimeService.SecondUserJoinedAsync(game.OwnerId, pacman.XCoordinate, pacman.YCoordinate);

                return;
            }

            await _realtimeService.JoinedToGameAsync(userConnectionId, pacman, game);
        }

        public async Task ChangeGameModeAsync(string gameId, string userId)
        {
            var game = (Game)_memoryCache.Get(gameId);
            game.PauseMode = !game.PauseMode;
            _memoryCache.Set(gameId, game);

            if (!game.PauseMode)
            {
                await StartGameAsync(userId, gameId, true);
            }
        }

        private async Task GameOverAsync(string userId, Game game)
        {
            await _realtimeService.GameOverAsync(userId, false);

            if (game.GameType == GameType.TwoPlayers)
            {
                var secondPacman = game.GameParticipants
                    .Select(x => x.GetType() == typeof(Pacman) && ((Pacman)x).UserId != userId ? (Pacman)x : null)
                    .First(p => p != null);
                await Task.Delay(200);
                await _realtimeService.GameOverAsync(secondPacman.UserId, true);
            }
        }

        private async Task RunGhostAsync(string gameId)
        {
            var game = (Game)await Task.Run(() => _memoryCache.Get(gameId));

            var ghosts = game.GameParticipants.Where(x => x.CreatureType != CreatureType.Pacman).ToList();

            foreach (var ghost in ghosts)
            {
                Task.Run(async () => { await MoveGhostAsync(gameId, ghost.CreatureType); });
            }
        }

        private static string GetPacmanId(int xCoordinate, int yCoordinate, Game game)
        {
            var pacman = (Pacman)game.GameParticipants.Find(x =>
                x.CreatureType == CreatureType.Pacman && x.XCoordinate == xCoordinate && x.YCoordinate == yCoordinate);

            return pacman.UserId;
        }

        private static void AddGhostsToGame(Game game, string pacmanId)
        {
            var redGhost = new RedGhost(5, 11, game.Id, CreatureType.RedGhost, Direction.Left);
            var greenGhost = new GreenGhost(7, 10, game.Id, CreatureType.GreenGhost, Direction.Top);

            game.Level[redGhost.XCoordinate, redGhost.YCoordinate] = FieldType.Ghost;
            game.Level[greenGhost.XCoordinate, greenGhost.YCoordinate] = FieldType.Ghost;
            game.GameParticipants.Add(redGhost);
            game.GameParticipants.Add(greenGhost);
        }

        private static Pacman AddPacmanToGame(int xCoordinate, int yCoordinate, string userId,
            PacmanColor color, Direction direction, Game game)
        {
            var pacman = new Pacman(xCoordinate, yCoordinate, game.Id, userId, color, direction);
            game.Level[xCoordinate, yCoordinate] = FieldType.Pacman;
            game.GameParticipants.Add(pacman);

            return pacman;
        }
    }
}
