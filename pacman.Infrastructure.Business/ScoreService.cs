﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using pacman.Domain.Entities.Infrastructure;
using pacman.Domain.Entities.Models;
using pacman.Domain.Interfaces;
using pacman.Services.Interfaces;

namespace pacman.Infrastructure.Business
{
    public class ScoreService : IScoreService
    {
        private readonly IScoreRepository _scoreRepository;
        private readonly IUserRepository _userRepository;

        public ScoreService(IUserRepository userRepository, IScoreRepository scoreRepository)
        {
            _userRepository = userRepository;
            _scoreRepository = scoreRepository;
        }

        public async Task<DatabaseResult> AddScoreAsync(string userId, Score score)
        {
            score.UserNickName = await _userRepository.GetUserNickNameAsync(userId);

            var result = await _scoreRepository.AddScoreAsync(score);

            return result;
        }

        public async Task<List<Score>> GetScoresAsync(Expression<Func<Score, object>> sortedExpression, int limit, int skip = 0)
        {
            var result = await _scoreRepository.GetScoresAsync(sortedExpression, limit, skip);

            return result;
        }
    }
}
