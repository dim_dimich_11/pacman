﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using pacman.Domain.Entities.Infrastructure;

namespace pacman.Domain.Interfaces.Infrastructure
{
    public interface IRepository<TEntity> where TEntity : class
    {
        Task<TEntity> FindOneAsync(Expression<Func<TEntity, bool>> predicate);

        Task<TResult> FindOneAsync<TResult>(Expression<Func<TEntity, bool>> predicate,
            Expression<Func<TEntity, TResult>> projection);

        Task<List<TEntity>> FindManyAsync(Expression<Func<TEntity, bool>> predicate, int limit = 20, int skip = 0);

        Task<List<TEntity>> FindManyWithOrderByDescendingAsync(Expression<Func<TEntity, bool>> predicate,
            Expression<Func<TEntity, object>> sortExpression, int limit = 20, int skip = 0);
        Task<DatabaseResult> InsertOneAsync(TEntity entity);
        Task<DatabaseResult> DeleteOneAsync(Expression<Func<TEntity, bool>> predicate);
        Task<DatabaseResult> UpdateOneAsync(TEntity entity);
    }
}
