﻿using pacman.Domain.Entities.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace pacman.Domain.Interfaces.Infrastructure
{
    public abstract class GenericRepository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        private readonly DbContext _context;
        private readonly DbSet<TEntity> _dbSet;

        protected GenericRepository(DbContext context)
        {
            _context = context;
            _dbSet = _context.Set<TEntity>();
        }

        public async Task<TEntity> FindOneAsync(Expression<Func<TEntity, bool>> predicate)
        {
            var result = await _dbSet.Where(predicate).FirstOrDefaultAsync(predicate);

            return result;
        }

        public async Task<TResult> FindOneAsync<TResult>(Expression<Func<TEntity, bool>> predicate, Expression<Func<TEntity, TResult>> projection)
        {
            var result = await _dbSet.Where(predicate).Select(projection).FirstOrDefaultAsync();

            return result;
        }

        public async Task<List<TEntity>> FindManyAsync(Expression<Func<TEntity, bool>> predicate, int limit = 20, int skip = 0)
        {
            var result = await _dbSet.Where(predicate).OrderBy(predicate).Skip(skip).Take(limit).ToListAsync();

            return result;
        }

        public async Task<List<TEntity>> FindManyWithOrderByDescendingAsync(Expression<Func<TEntity, bool>> predicate,
            Expression<Func<TEntity, object>> sorExpression, int limit = 20, int skip = 0)
        {
            var result = await _dbSet.OrderByDescending(sorExpression).Where(predicate).ToListAsync();

            return result;
        }

        public async Task<DatabaseResult> InsertOneAsync(TEntity entity)
        {
            var dbResult = new DatabaseResult();
            try
            {
                _dbSet.Add(entity);
                await _context.SaveChangesAsync();
                dbResult.Success = true;
                return dbResult;
            }
            catch (Exception exp)
            {
                dbResult.Message = "Exception in InserOneAsync method.";
                dbResult.Exception = exp;
                return dbResult;
            }
        }

        public async Task<DatabaseResult> DeleteOneAsync(Expression<Func<TEntity, bool>> predicate)
        {
            var dbResult = new DatabaseResult();
            try
            {
                var result = await _dbSet.FirstOrDefaultAsync(predicate);
                if (result != null)
                {
                    _dbSet.Remove(result);
                    await _context.SaveChangesAsync();
                    dbResult.Success = true;
                    return dbResult;
                }

                dbResult.Message = "Error: Nothing was found by expression";
                return dbResult;
            }
            catch (Exception exp)
            {
                dbResult.Message = "Exception in DeleteOneAsync method.";
                dbResult.Exception = exp;
                return dbResult;
            }
        }

        public async Task<DatabaseResult> UpdateOneAsync(TEntity entity)
        {
            var dbResult = new DatabaseResult();
            try
            {
                _context.Entry(entity).State = EntityState.Modified;
                await _context.SaveChangesAsync();

                dbResult.Success = true;
                return dbResult;
            }
            catch (Exception exp)
            {
                dbResult.Message = "Exception in UpdateOneAsync method.";
                dbResult.Exception = exp;
                return dbResult;
            }

        }
    }
}
