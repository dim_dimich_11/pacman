﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using pacman.Domain.Entities.Infrastructure;
using pacman.Domain.Entities.Models;
using pacman.Domain.Interfaces.Infrastructure;

namespace pacman.Domain.Interfaces
{
    public interface IScoreRepository : IRepository<Score>
    {
        Task<DatabaseResult> AddScoreAsync(Score score);
        Task<List<Score>> GetScoresAsync(Expression<Func<Score, object>> sortedExpression, int limit, int skip);
    }
}
