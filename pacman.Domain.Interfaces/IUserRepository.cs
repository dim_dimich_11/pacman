﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using pacman.Domain.Entities.Infrastructure;
using pacman.Domain.Entities.Models;
using pacman.Domain.Interfaces.Infrastructure;

namespace pacman.Domain.Interfaces
{
    public interface IUserRepository : IRepository<ApplicationUser>
    {
        Task<ApplicationUser> GetUserAsync(string userId);
        Task<ApplicationUser> GetUserByNickNameAsync(string nickName);
        Task<DatabaseResult> UpdateUserAsync(ApplicationUser user);
        Task<string> GetUserNickNameAsync(string userId);
    }
}
